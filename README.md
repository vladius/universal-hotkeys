# Introduction

Using lots of different software everyday, it's really hard to remember all the hotkeys used for common operations. This project aims to eliminate the difficulties by specifying a common standard that would span across the applications.


# Goals

Be consistent and comfortable for everyday usage. No conflicts are allowed.
The design should be clear to understand for the end user and application
developers. Historical conventions should be respected, popular software
shortcuts should be considered for inclusion.


# Scope

The current version of this standard is for PC-based QWERTY keyboards with `Ctrl` keys and numeric pad (numpad) presented.

The main target are multimedia, digital content creation applications both in 2D and 3D. This standard is not for text editors and the likes.

The hotkeys are currently optimized for the left hand being on left side of the keyboard and the right hand resting on a mouse.


# Terminology Used

**User** – someone, who is currently executing a shortcut.

**Application** – a software package that implements the Universal Hotkeys standard.

**Document** – a piece of data, that can be loaded into the Application, possibly edited in the Application and possible saved on a hard drive (or other media) afterwards.

**Command** – the executed command, triggered by the hotkey.

**Behavior** – the expected behavior of the Application. The actual behavior may differ.


# Shortenings

Certain key and button names are shortened. These are:

| Key/Button | Meaning |
| ---------- | ------- |
| <kbd>LMB</kbd> | Left mouse button click. |
| <kbd>MMB</kbd> | Middle mouse button click. |
| <kbd>RMB</kbd> | Right mouse button click. |
| <kbd>MM</kbd> | Mouse (Cursor) motion. |
| <kbd>MWU</kbd> | Mouse wheel upward roll. |
| <kbd>MWD</kbd> | Mouse wheel downward rool. |
| <kbd>Num0</kbd> | Numpad's `0` (zero) key. |
| <kbd>Num1</kbd> | Numpad's `1` (one) key. |
| <kbd>Num2</kbd> | Numpad's `2` (two) key. |
| <kbd>Num3</kbd> | Numpad's `3` (three) key. |
| <kbd>Num4</kbd> | Numpad's `4` (four) key. |
| <kbd>Num5</kbd> | Numpad's `5` (five) key. |
| <kbd>Num6</kbd> | Numpad's `6` (six) key. |
| <kbd>Num7</kbd> | Numpad's `7` (seven) key. |
| <kbd>Num8</kbd> | Numpad's `8` (eight) key. |
| <kbd>Num9</kbd> | Numpad's `9` (nine) key. |
| <kbd>Num/</kbd> | Numpad's `/` (forward slash/divide) key. |
| <kbd>Num*</kbd> | Numpad's `*` (asteriks/multiply) key. |
| <kbd>Num-</kbd> | Numpad's `-` (hyphen/minus) key. |
| <kbd>Num+</kbd> | Numpad's `+` (plus) key. |
| <kbd>NumEnter</kbd> | Numpad's `Enter` (Return) key. |
| <kbd>Num.</kbd> | Numpad's `.` (dot/point) key. |
| <kbd>NumLk</kbd> | Numpad's lock key. |


# Preferred Key Semantics

A certain semantics is preferred when designing a Shortcut.
The <kbd>Ctrl</kbd> modifier key should be related to some sort of modification 
or selection. This also is associated with some kind of snapping and/or
constraints.
The <kbd>Shift</kbd> modifier key should be related to some greater or more 
accented behavior.
The <kbd>Alt</kbd> modifier should reverse the effect of the operation or be an
alternative to it. It is also the central key for view manipulation.


# Design Recommendations

Certain recommendations are provided for designing new shortcuts.

## Common Software Conventions

Common operations and their hotkey associations should be left intact among all
software, be it a web browser or a 3D modelling application. An F11 key, for
example is seldomly used hotkey for full-screen mode.


# Hotkeys

## Application-Level

Hotkeys for dealing with the Application in general.

| Shortcut | Command | Behavior | Rationale |
| -------- | ------- | -------- | --------- |
| <kbd>Ctrl</kbd> + <kbd>Q</kbd> | Quit | Quits the Application, possibly asking a User to save a changed Document. | |
| <kbd>F1</kbd> | Help | Normally opens the main Application manual. | |
| <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>P</kbd> | Preferences | Opens the Application's main settings. | |


### Application-Level Presentation

Hotkeys related to the Application presentation in general.

| Shortcut | Command | Behavior | Rationale |
| -------- | ------- | -------- | --------- |
| <kbd>F11</kbd> | Fullscreen | Enters a fullscreen mode, removing any window system's borders and headers. | Very common. |


## Document-Level

These hotkeys are for dealing with Document(s).

| Shortcut | Command | Behavior | Rationale |
| -------- | ------- | -------- | --------- |
| <kbd>Ctrl</kbd> + <kbd>Q</kbd> | New... | Creates a new document in the Application. | Very common. |
| <kbd>Ctrl</kbd> + <kbd>O</kbd> | Open.. | Opens a file browser window to choose a file to load into the Application | Very common. |
| <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>O</kbd> | Open Recent... | Opens a list of recent Documents to choose from to load into the Application. | Derived from the "Open..." shortcut. |
| <kbd>Ctrl</kbd> + <kbd>S</kbd> | Save | If the Document is not already saved on disk, the `Save Document As` should normally be executed. | Very common. |
| <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>S</kbd> | Save As... | Opens a file browser window to choose a file to write the contents of the active Document. | Very common. |
| <kbd>Ctrl</kbd> + <kbd>W</kbd> | Close | Closes the active Document. If there is no active Document currently opened or no multi-document editing supported, the Application should normally quit. | Very common. |


## Action History

Hotkeys for dealing with the linear action history.

| Shortcut | Command | Behavior | Rationale |
| -------- | ------- | -------- | --------- |
| <kbd>Ctrl</kbd> + <kbd>Z</kbd> | Undo | Rollbacks the effect of the last action. | |
| <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>Z</kbd>, <kbd>Ctrl</kbd> + <kbd>Y</kbd> | Redo | Re-does the effect of the last un-done action. | |
| <kbd>Ctrl</kbd> + <kbd>H</kbd> | History... | Shows the action history on screen and reverts to a chosen step. | |
| <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>H</kbd> | Repeat From History... | Shows the history and repeats a chosen action from it. | |


## Active Context

Hotkeys related to some active context (a sub-region or a sub-window) that is 
a part of the Application.

| Shortcut | Command | Behavior | Rationale |
| -------- | ------- | -------- | --------- |
| <kbd>Ctrl</kbd> + <kbd>F11</kbd> | Fullscreen Context | Makes the active context's area maximized. | Derived from the "Fullscreen" shortcut. |
| <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>F11</kbd> | Fullscreen Context More | Makes the active context's area maximized and menu-free. | Derived from the "Fullscreen Context" shortcut. |


## Windowing System

Hotkeys for dealing with the GUI sytem of the Application.

| Shortcut | Command | Behavior | Rationale |
| -------- | ------- | -------- | --------- |
| <kbd>MWU</kbd> | Scroll Up | Scroll the area up. | Natural scroll-wheel functionality. |
| <kbd>MWD</kbd> | Scroll Down | Scroll the area down. | Natural scroll-wheel functionality. |
| <kbd>MWU</kbd> | Scroll Up Page | Scroll the area up more. | Natural key behavior. |
| <kbd>MWD</kbd> | Scroll Down Page| Scroll the area down more. | Natural key behavior. |
| <kbd>Shift</kbd> + <kbd>MWU</kbd> | Scroll Right | Scroll the area right. | Common browser behavior (found in Chrome). |
| <kbd>Shift</kbd> + <kbd>MWD</kbd> | Scroll Left | Scroll the area left. | Common browser behavior (found in Chrome). |
| <kbd>Ctrl</kbd> + <kbd>+</kbd> | Zoom In | Zoom in the area. | Wide-spread browser behavior. |
| <kbd>Ctrl</kbd> + <kbd>-</kbd> | Zoom Out | Zoom out the area. | Wide-spread browser behavior. |
| <kbd>Ctrl</kbd> + <kbd>Num+</kbd> | Zoom In | Zoom in the area. | Wide-spread browser behavior. |
| <kbd>Ctrl</kbd> + <kbd>Num-</kbd> | Zoom Out | Zoom out the area. | Wide-spread browser behavior. |
| <kbd>Ctrl</kbd> + <kbd>0</kbd> | Zoom Reset | Reset the zooming ratio. | Wide-spread browser behavior. |


## Selection

Selection is an important aspect of software that let's you focus on or apply
the effects of operations only to certain objects. Here are the hotkeys:

| Shortcut | Command | Behavior | Rationale |
| -------- | ------- | -------- | --------- |
| <kbd>Ctrl</kbd> + <kbd>A</kbd> | Select All | Select all of the available objects. | Very common. |
| <kbd>Ctrl</kbd> + <kbd>D</kbd> | Select None | Deselect all of the objects. | Very common. |
| <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>I</kbd> | Invert Selection | Invert the current selection. | Common. |
| <kbd>+</kbd>, <kbd>Num+</kbd> | Select More | Add adjacent elements/objects to the active selection. | Mnemonic to plus. |
| <kbd>-</kbd>, <kbd>Num+</kbd> | Select Less | Shrink the active selection be removing elments/objects on its borders. | Mnemonic to minus. |
| <kbd>Ctrl</kbd> + <kbd>LMB</kbd> | Add To Selection | Add pointed element to selection. | File browser behavior. |


## 2D Viewport

General hotkeys for the 2D viewports like UV editors, image viewers,
node graphs, etc.


### 2D Viewport Navigation

Navigating the 2D viewport.

| Shortcut | Command | Behavior | Rationale |
| -------- | ------- | -------- | --------- |
| <kbd>Alt</kbd> + <kbd>LMB</kbd> + <kbd>MM</kbd> | Rotate | Change the orientation of the view. | |
| <kbd>Alt</kbd> + <kbd>MMB</kbd> + <kbd>MM</kbd> | Pan | Change the viewing position. | |
| <kbd>Alt</kbd> + <kbd>RMB</kbd> + <kbd>MM</kbd> | Zoom | Zoom the view in or out. | |
| <kbd>Num+</kbd> | Zoom In | Zoom the view in a bit. | |
| <kbd>Num-</kbd> | Zoom Out | Zoom the view out a bit. | |
| <kbd>Num.</kbd> | Focus | Focus the camera on selected elements. | |
| <kbd>Alt</kbd> + <kbd>B</kbd> | Zoom to Boundary | Zoom in to a drawn region. | |


#### 2D Viewport Scrolling

| Shortcut | Command | Behavior | Rationale |
| -------- | ------- | -------- | --------- |
| <kbd>Ctrl</kbd> + <kbd>MWU</kbd> | Pan Up | Pan the viewing position up a bit. | |
| <kbd>Ctrl</kbd> + <kbd>MWD</kbd> | Pan Down | Pan the viewing position down a bit. | |
| <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>MWU</kbd> | Pan Right | Pan the viewing position right a bit. | |
| <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>MWD</kbd> | Pan Left | Pan the viewing position left a bit. | |


## 3D Viewport

General hotkeys during the 3D viewport navigation.

| Shortcut | Command | Behavior | Rationale |
| -------- | ------- | -------- | --------- |
| <kbd>Num5</kbd> | Orthographic | Set the view to orthographic projection. | |
| <kbd>Alt</kbd> + <kbd>Num5</kbd> | Perspective | Set the view to perspective projection. | |
| <kbd>Ctrl</kbd> + <kbd>Num5</kbd> | Perspective/Orthographic | Switch between orthographic/perspective projections. | |


### 3D Viewport Navigation

Navigating the viewport.

| Shortcut | Command | Behavior | Rationale |
| -------- | ------- | -------- | --------- |
| <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>F</kbd> | FPS Mode | Enter the "first person shooter" mode. | |
| <kbd>Alt</kbd> + <kbd>LMB</kbd> + <kbd>MM</kbd> | Orbit | Change the orientation and position of the camera around the focus point. | | 
| <kbd>Alt</kbd> + <kbd>MMB</kbd> + <kbd>MM</kbd> | Pan | Change the position of the camera in the current view plane. | |
| <kbd>Alt</kbd> + <kbd>RMB</kbd> + <kbd>MM</kbd> | Zoom | Zoom in/out to/from the focust point. | |
| <kbd>Alt</kbd> + <kbd>Shift</kbd> + <kbd>LMB</kbd> + <kbd>MM</kbd> | Roll | Roll the view around the view center. | An alternative to Orbit. | 
| <kbd>Num+</kbd> | Zoom In | Zoom the view in a bit. | |
| <kbd>Num-</kbd> | Zoom Out | Zoom the view out a bit. | |
| <kbd>Num.</kbd> | Focus | Focus the camera on selected elements. | |
| <kbd>Num0</kbd> | View From Camera | Change the view to that of the active scene camera. | |


### Predefined 3D View Angles

Accessing certain predefined view angles.

#### Global Space

| Shortcut | Command | Behavior | Rationale |
| -------- | ------- | -------- | --------- |
| <kbd>Num4</kbd> | Left | View *from* the left side of the world. | |
| <kbd>Num6</kbd> | Right | View *from* the right side of the world. | |
| <kbd>Num2</kbd> | Front | View *from* the front of the world. | |
| <kbd>Num8</kbd> | Back | View *from* the back of the world. | |
| <kbd>Num7</kbd> | Top | View *from* top of the world. | |
| <kbd>Num1</kbd> | Bottom | View *from* bottom of the world. | |


#### Local Space

| Shortcut | Command | Behavior | Rationale |
| -------- | ------- | -------- | --------- |
| <kbd>Ctrl</kbd> + <kbd>Num4</kbd> | Left | View *from* the left side of the object. | |
| <kbd>Ctrl</kbd> + <kbd>Num6</kbd> | Right | View *from* the right side of the object. | |
| <kbd>Ctrl</kbd> + <kbd>Num2</kbd> | Front | View *from* the front of the object. | |
| <kbd>Ctrl</kbd> + <kbd>Num8</kbd> | Back | View *from* the back of the object. | |
| <kbd>Ctrl</kbd> + <kbd>Num7</kbd> | Top | View *from* top of the object. | |
| <kbd>Ctrl</kbd> + <kbd>Num1</kbd> | Bottom | View *from* bottom of the object. | |


## 3D Mesh Modelling

These hotkeys are for 3D mesh modelling context.


### Element Manipulation Context

| Shortcut | Command | Behavior | Rationale |
| -------- | ------- | -------- | --------- |
| <kbd>1</kbd> | Vertices | Manipulate mesh vertices. | Used very often. Has to be accessed easily with a left hand. |
| <kbd>2</kbd> | Edges | Manipulate mesh edges. | Used very often. Has to be accessed easily with a left hand. |
| <kbd>3</kbd> | Faces | Manipulate mesh faces. | Used very often. Has to be accessed easily with a left hand. |


### Selection

| Shortcut | Command | Behavior | Rationale |
| -------- | ------- | -------- | --------- |
| <kbd>Shift</kbd> + <kbd>LMB</kbd> | Select Shortest Path | Select elements (vertices) on the shortest path between selected and picked element (vertex). | File browser-like behavior. |
| 2x<kbd>LMB</kbd>,<kbd>Ctrl</kbd> + <kbd>L</kbd> | Select Edge Loop | Select an edge loop. | Edge loops are often used. |
| 2x<kbd>MMB</kbd>,<kbd>Ctrl</kbd> + <kbd>R</kbd> | Select Edge Ring | Select an edge ring. | Edge loops are quite often used. |


### Modelling

| Shortcut | Command | Behavior | Rationale |
| -------- | ------- | -------- | --------- |
| <kbd>X</kbd> | Extrude | Extrude the mesh geometry. | Used very often. Has to be accessed easily with a left hand. Mnemonic: "X" is pronounced like the first two letters of "extrude". |
| <kbd>Alt</kbd> + <kbd>X</kbd> | Extrusion Variants | Extrude the mesh geometry with a specific algorithm/mode. | Mnemonic: "Alt" is for alternate, "X" is pronounced like the first two letters of "extrude". |
